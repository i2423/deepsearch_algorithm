#this file is a DeepSearch algorithm - implemented by: AndresFWilT
#first, we will create our dicctionary of the tree
graph = {"A" : ["C","B"],
            "B" : ["D","E"],
            "C" : ["F","G"],
            "D" : ["H"],
            "E" : ["I"],
            "F" : ["J"],
            "G" : ["K"],
            "H" : ["L","M"],
            "I" : ["N"],
            "J" : ["O","P"],
            "K" : ["Q","R","S"],
            "L" : ["T"],
            "M" : ["U"],
            "N" : ["V"],
            "O" : ["W"],
            "P" : ["X"],
            "Q" : ["Y"],
            "R" : ["Z"],
            "S" : ["AA"],
            "T" : ["AB","AC"],
            "U" : ["AD","AE"],
            "V" : ["AF","AG"],
            "W" : ["AH","AI"],
            "X" : ["AJ","AK"],
            "Y" : ["AL","AM"],
            "Z" : ["AN","AO"],
            "AA" : ["AP","AQ"],
}

#Recursive function to go trough all the tree and print from the node that recieve the method
def dfs_non_recursive(graph, source):   
    if source is None or source not in graph:
        return "Invalid input"
    path = []
    stack = [source]
    while(len(stack)!=0):
        s = stack.pop()
        if s not in path:
            path.append(s)
        if s not in graph:
            #this is a leaf node
            continue
        for neighbor in graph[s]:
            stack.append(neighbor)
    return " ".join(path)

#dfspath, variable that contains de tree (graph - dicctionary) and a specific node (A)
dfsPath = dfs_non_recursive(graph,"A")
#printing
print(dfsPath)